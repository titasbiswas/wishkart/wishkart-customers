package com.wishkart.customers.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wishkart.customers.model.Address;
import com.wishkart.customers.repositories.AddressRepository;

@RestController
@RequestMapping("/cust")
public class CustController {
	
	@Autowired
	private AddressRepository addressRepo;
	
	@GetMapping(value="/{id}", produces=MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<Address> getAddressById(@PathVariable("id") Long addressId) {
		Optional<Address> optAddress =  addressRepo.findById(addressId);
		if(optAddress.isPresent())
			return ResponseEntity.ok(optAddress.get());
		else return null;
	}

}
