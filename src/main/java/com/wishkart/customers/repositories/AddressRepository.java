package com.wishkart.customers.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wishkart.customers.model.Address;

public interface AddressRepository extends JpaRepository<Address, Long> {

}
