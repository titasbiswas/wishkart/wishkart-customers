package com.wishkart.customers.model;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.GeneratorType;

import com.wishkart.customers.model.Address.AddressBuilder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;


/**
 * The persistent class for the users database table.
 * 
 */
@Entity
@Table(name="users")
@NamedQuery(name="User.findAll", query="SELECT u FROM User u")
@Data
@Builder
@AllArgsConstructor
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String password;
	
	@Column(name="user_name")
	private String userName;

	//bi-directional many-to-one association to Address
	@OneToMany(mappedBy="user", fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@Builder.Default
	private List<Address> addresses = new ArrayList<>();

	public User() {
	}

	

	public Address addAddress(Address address) {
		getAddresses().add(address);
		address.setUser(this);

		return address;
	}

	public Address removeAddress(Address address) {
		getAddresses().remove(address);
		address.setUser(null);

		return address;
	}

}