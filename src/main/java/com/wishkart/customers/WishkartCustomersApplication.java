package com.wishkart.customers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WishkartCustomersApplication {

	public static void main(String[] args) {
		SpringApplication.run(WishkartCustomersApplication.class, args);
	}
}
